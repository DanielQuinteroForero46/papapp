import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './UI/common/helpers/auth.guard';
import { LoginComponent } from './UI/view-models/auth/login/login.component';
import { RegisterComponent } from './UI/view-models/auth/register/register.component';
import { ViewContainerComponent } from './UI/view-models/view-container/view-container.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./UI/common/modules/view-container.module').then(m => m.ViewContainerModule),
      component: ViewContainerComponent,
      // canActivate: [AuthGuard],
  },
  { path: 'auth/login', component: LoginComponent, data: { title: 'Login' } },
  { path: 'auth/register', component: RegisterComponent, data: { title: 'Register' } },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
