import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError } from 'rxjs/operators';
import { AuthService } from "../drive-adapter/auth-service/auth.service";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private accountService: AuthService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            console.log(request);
            let errorMessage : any = "Se ha presentado un inconveniente interno en el servicio. Por favor intenta nuevamente.";
            switch(err.status) {
                case 400:
                    if(typeof err?.error?.errors! == "object") {
                        errorMessage = "";
                        Object.values(err.error.errors).forEach((message : any) => {
                            errorMessage += message ?? "" + "<br>";
                        });
                    }
                    break;
                case 401 || 403:
                    this.accountService.Logout();
                    break;
                case 404:
                    errorMessage = (err && err.error && err.error.errors)? err.error.errors[0].detail : "";
                    break;
            }
            return throwError(errorMessage);
        }))
    }
}