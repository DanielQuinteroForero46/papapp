import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { AuthService } from "../drive-adapter/auth-service/auth.service";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add auth header with jwt if account is logged in and request is to the api url
        const account = this.authService.accountValue;
        const isLoggedIn = account && account.userLogin?.token!;
        const isApiUrl = request.url.startsWith(environment.apiUrl);

        if (isLoggedIn && isApiUrl) {
            request = request.clone({
                setHeaders: { 
                    Authorization: `Bearer ${account?.userLogin?.token!}`,
                    'X-Forwarded-For': this.authService?.IP,
                },
            });
        }
        //Set last activity for validate idle time when the token expires
        this.authService.SetLastActivity();
        return next.handle(request);
    }
}