import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { IFases } from 'src/app/domain/interfaces/fases/interface-fases';
import { FasesGateway } from 'src/app/domain/models/fases/gateway/fases-gateway';
import { CurrencyPipe } from '@angular/common';
import { TIPO_CALCULO } from 'src/app/domain/enums/fases-cultivo-enum';

const fases_key_storage = "fases-cultivo";
@Injectable({
  providedIn: 'root'
})
export class FasesService extends FasesGateway {
  private fasesSubject!: BehaviorSubject<IFases>;

  constructor(private currencyPipe? : CurrencyPipe) { 
    super();
    this.fasesSubject = new BehaviorSubject<IFases>(JSON.parse(localStorage.getItem(fases_key_storage)??"{}"));
  }

  public get getFases(): IFases {
    return this.fasesSubject?.value;
  }

  public AlmacenarFases(fases : IFases): boolean {
    this.fasesSubject?.next(fases);
    console.log(fases);
    localStorage.setItem(fases_key_storage, JSON.stringify(fases)??"");
    return true;
  }

  public async PrecargarForm(FormGroup : FormGroup, FormModel : Object) : Promise<FormGroup> {
    FormGroup.patchValue(FormModel);
    Object.keys(FormGroup.getRawValue()).map((ControlName : string) => {
      setTimeout(() => {
        // console.log(`${ControlName}: ${FormGroup.get(ControlName)!.value}`);
        let input : HTMLInputElement = (document.getElementById(ControlName) as HTMLInputElement);
        if(input) {
          if(FormGroup.get(ControlName)!.value) {
            if(input.type == "text") input.value = this.TransformAmount(FormGroup.get(ControlName)?.value!);
          } 
          else input.value = "";
        }
      }, 500);
    });
    return FormGroup;
  }

  public TransformAmount(value : string) : any {
    let controlValue : string = value?.toString();
    if(!isNaN(parseInt(controlValue)) && controlValue?.length > 0) {
      return this.currencyPipe!.transform(controlValue, '$')!;
    }
    return "";
  }

  CalcularTotal(valores : Array<number>, TipoCalculo : TIPO_CALCULO) : number {
    let total : number = 0;
    valores.forEach((valor : number) => {
      if(valor) {
        switch(TipoCalculo) {
          case TIPO_CALCULO.SUMA:
            total+= parseInt(valor.toString());
            break;
          case TIPO_CALCULO.RESTA:
            total-= valor;
            break;
          case TIPO_CALCULO.PRODUCTO:
            total = (total === 0)? valor : (total * valor);
            break;
          case TIPO_CALCULO.DIVISION:
            total/= valor;
            break;
          default:
            total+= parseInt(valor.toString());
        }
      }      
    });
    return total;
  }
  
}