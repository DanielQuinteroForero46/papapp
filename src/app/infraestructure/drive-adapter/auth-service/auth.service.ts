import { Injectable } from '@angular/core';
import { IUser, IUserLogin } from 'src/app/domain/interfaces/interface-auth';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AlertType } from 'src/app/domain/enums/alert-type-enum';
import { AlertService } from '../alert-service/alert.service';
import { AuthGateway } from 'src/app/domain/models/users/gateway/auth-gateway';

const user_key_storage = 'auth-user-papapp';
@Injectable({
  providedIn: 'root'
})
export class AuthService extends AuthGateway {
  private accountSubject!: BehaviorSubject<IUser>;
  public account!: Observable<IUser>;
  private refreshTokenTimeout!: ReturnType<typeof setTimeout>;
  public IP: string = "";

  constructor(private router: Router, private http: HttpClient, private alertService: AlertService) { 
    super();
    this.accountSubject = new BehaviorSubject<IUser>(this.GetUserFromStorage()!);
    this.account = this.accountSubject.asObservable();
  }
  
  public get accountValue(): IUser {
    return this.accountSubject?.value;
  }

  Register(user: IUser) : Observable<IUser> {
    return this.http.post<IUser>(`${environment.apiUrl}/api/User/Register`, user)
      .pipe(map((registerResponse: IUser) => {
        return registerResponse;
      }));
  }

  Login(userLogin : IUserLogin) : Observable<IUser> {
    return this.http.post<IUser>(`${environment.apiUrl}/api/User/Authenticate`, userLogin)
      .pipe(map((loginResponse : IUser) => {
        this.accountSubject?.next(loginResponse);
        let user = this.accountSubject.value!;
        this.SaveUserToStorage(user);
        return loginResponse;
      }));
  }

  Logout(): void {
    localStorage.clear();
    this.accountSubject.next(null!);
    clearTimeout(this.refreshTokenTimeout);
    this.router.navigate(["/auth/login"]);
  }

  GetLastActivity() : string {
    let activity = sessionStorage.getItem("lastActivityDate");
    if(activity!=null){
      return activity.toString();
    }
    return "";
  }

  SetLastActivity() : void {
    sessionStorage.setItem("lastActivityDate", new Date().toUTCString());
  }

  RefreshToken() {
    // var refreshToken=this.tokenService.getrefreshToken();
    // var expiredToken=this.tokenService.getToken();
    // const headers = new HttpHeaders()
    //   .append('refresh-token', `${refreshToken}`)
    //   .append('token', `${expiredToken}`);
    // return this.http.post<any>(`${environment.apiUrl}/api/user/refreshtoken`,null,{ headers: headers })
    //   .subscribe((account) => {
    //     this.accountSubject.next(account);
    //     // this.tokenService.saveToken(this.accountSubject.value.UserLogin?.Token!);
    //     // this.tokenService.saveUser(this.accountSubject.value);
    //     // this.tokenService.saverefreshToken(this.accountSubject.value.UserLogin?.RefreshToken!);
    //     this.StartRefreshTokenTimer();
    //     return account;
    //   });
  }

  ValidateDownTime() {
    let lastActivity  = new Date(this.GetLastActivity());
    const lastActivityUTC = new Date(new Date(lastActivity).getUTCFullYear(), 
    new Date(lastActivity).getUTCMonth(), new Date(lastActivity).getUTCDate(), 
    new Date(lastActivity).getUTCHours(), (new Date(lastActivity).getUTCMinutes()), 
    new Date().getUTCSeconds());

    const inactivityDeadlineUTC = new Date(
      new Date().getUTCFullYear(), new Date().getUTCMonth(), new Date().getUTCDate(), 
      new Date().getUTCHours(), (new Date().getUTCMinutes()-environment.maxIdleTimeMinutes), 
      new Date().getUTCSeconds());

    if(lastActivityUTC < inactivityDeadlineUTC) {
      this.alertService.showAlert({
        Type: AlertType.Warning,
        Description: "Se ha alcanzado el tiempo máximo de inactividad.<br>Ingresa nuevamente.",
        AutoClose: false,
        ShowAsModal: true
      });
      this.Logout();
    } else {
      this.RefreshToken();
    }
  }

  public StartRefreshTokenTimer() {
    // parse json object from base64 encoded jwt token
    const jwtToken = JSON.parse(atob(this.accountValue?.userLogin?.token!.split('.')[1]));
    let tokenExpiration = new Date(jwtToken.exp * 1000);
    // set a timeout to refresh the token a minute before it expires
    const timeout = tokenExpiration.getTime() - Date.now() - (60 * 1000);
    this.refreshTokenTimeout = setTimeout(() => this.ValidateDownTime(), timeout);
  }

  SaveUserToStorage(user: IUser): void {
    localStorage.removeItem(user_key_storage);
    localStorage.setItem(user_key_storage, JSON.stringify(user)??"");
  }
  
  GetUserFromStorage() : IUser | null {
    const user = localStorage.getItem(user_key_storage);
    if(user) {
      return JSON.parse(user);
    }
    return null;
  }
}
