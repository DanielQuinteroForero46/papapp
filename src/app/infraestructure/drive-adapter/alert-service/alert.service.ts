import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Alert } from '../../../domain/models/alert/alert';
import { AlertGateway } from '../../../domain/models/alert/gateway/alert-gateway';

@Injectable({
  providedIn: 'root'
})
export class AlertService extends AlertGateway {
  private subject = new Subject<Alert>();
  private defaultId = 'default-alert';

  onAlert(id: any): Observable<Alert> {
    return this.subject.asObservable().pipe(filter(x => x && x.Id === id));
  }
  // core alert method
  showAlert(alert: Alert) {
    alert.Id = alert.Id || this.defaultId;
    alert.AutoClose = (alert.AutoClose === undefined ? true : alert.AutoClose);
    this.subject.next(alert);
  }
  // clear alerts
  clear(Id = this.defaultId) {
    this.subject.next(new Alert({ Id }));
  }
}