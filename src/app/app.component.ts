import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'PapApp';
  constructor(private titleService : Title, private router: Router, private activePage: ActivatedRoute){
    
  }
  
  ngOnInit() {
    this.changeTitle();
  }
  
  changeTitle() {
    this.router.events.subscribe(event => {
      let title = "PapApp"
      switch (true) {
        case event instanceof NavigationEnd:
          let child = this.activePage.firstChild;
          if(child!=null) {
            while (child.firstChild) {
              child = child.firstChild;
            }
            title = child.snapshot.data.title;
          }
          this.titleService.setTitle(title + ' - PapApp');
          break;
        default:
          break;
      }
    });
  }
}
