import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './UI/view-models/auth/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RegisterComponent } from './UI/view-models/auth/register/register.component';
import { AlertComponent } from './UI/view-models/alert/alert.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './infraestructure/helpers/jwt.interceptor';
import { ErrorInterceptor } from './infraestructure/helpers/error.interceptor';
import { ViewContainerComponent } from './UI/view-models/view-container/view-container.component';
import { FasesCultivoComponent } from './UI/view-models/fases-cultivo/fases-cultivo.component';
import { HeaderComponent } from './UI/view-models/header/header.component';
import { FooterComponent } from './UI/view-models/footer/footer.component';
import { FaseSiembraComponent } from './UI/view-models/fases-cultivo/fase-siembra/fase-siembra.component';
import { CurrencyPipe } from '@angular/common';
import { FaseCrecimientoComponent } from './UI/view-models/fases-cultivo/fase-crecimiento/fase-crecimiento.component';
import { FaseDeshierbaComponent } from './UI/view-models/fases-cultivo/fase-deshierba/fase-deshierba.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    AlertComponent,
    ViewContainerComponent,
    FasesCultivoComponent,
    HeaderComponent,
    FooterComponent,
    FaseSiembraComponent,
    FaseCrecimientoComponent,
    FaseDeshierbaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule
  ],
  providers: [
    CurrencyPipe,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
