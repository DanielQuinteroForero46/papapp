import { FormControl, Validators } from "@angular/forms";
import { IField, IFieldVal } from "../../../domain/interfaces/interface-validation";

export const MoneyGeneralValidators : Array<IFieldVal> = [
    { Validation: Validators.min(0), ErrorMessage: "Valor incorrecto", ValidationName: Validators.min.name },
    { Validation: Validators.pattern(/^\d+$/), ErrorMessage: "Valor incorrecto", ValidationName: Validators.pattern.name },
    { Validation: Validators.max(1000000000), ErrorMessage: "Valor incorrecto", ValidationName: Validators.max.name },
    { Validation: Validators.maxLength(20), ErrorMessage: "Valor incorrecto", ValidationName: Validators.maxLength.name }
]

// Create FormControl with validations:
export function CreateField(Control : FormControl, Validations : Array<IFieldVal>, ElementID? : string) : IField {
    let Field = {
        ElementID: ElementID || "",
        Control: Control,
        Validations: Validations,
        ShowMsg: ""
    }
    Validations.map(val => { 
        val.ValidationName = (!val.ValidationName? val.Validation.name : val.ValidationName)?.toLowerCase();
        Field.Control.setValidators(Field.Control.validator? [Field.Control.validator, val.Validation] : val.Validation);
    });
    InitAndValidateForm(Field);
    return Field;
}

// Define event for field validations (ShowMsg = Message to show by field, in real time)
const InitAndValidateForm = (Field : IField) => {
    Field.Control?.valueChanges.pipe().subscribe(value => {
        if (Field.Control?.errors) {
            let error : string = Object.keys(Field.Control.errors)[0];
            Field.ShowMsg = Field.Validations.find((c : IFieldVal) => c.ValidationName === error)?.ErrorMessage || "";
        } 
        else Field.ShowMsg = "";
    });
}