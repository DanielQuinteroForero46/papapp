import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { AuthService } from "../../../infraestructure/drive-adapter/auth-service/auth.service";
import * as publicIp from 'public-ip';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private authService: AuthService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const account = this.authService.accountValue;
        publicIp.v4().then((ipResult : string) => { 
            this.authService.IP = ipResult;
        });

        if (account?.userLogin?.token) {
            this.authService.StartRefreshTokenTimer();
            return true;
        }

        this.router.navigate(['/auth/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }
}