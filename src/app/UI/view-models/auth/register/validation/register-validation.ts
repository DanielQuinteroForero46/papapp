import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from "@angular/forms";
import { CreateField } from "src/app/UI/common/helpers/form";
import { IField } from "../../../../../domain/interfaces/interface-validation";

export class RegisterForm {
    public Register : FormGroup;
    constructor() {
        // DEFINE REGISTER FORM:
        this.Register = new FormGroup({
            Nombres : this.Nombres.Control,
            Apellidos : this.Apellidos.Control,
            Correo : this.Correo.Control,
            Usuario : this.Usuario.Control,
            Contrasena : this.Contrasena.Control,
            ConfirmarContrasena : this.ConfirmarContrasena.Control,
        });
    }

    // DEFINE FORM FIELDS WITH VALIDATIONS:
    public Nombres : IField = CreateField(new FormControl(), [
        { Validation: Validators.required, ErrorMessage: "Ingresa tu nombre." },
        { Validation: Validators.minLength(3), ErrorMessage: "Nombre no válido", ValidationName: Validators.minLength.name },
        { Validation: Validators.maxLength(50), ErrorMessage: "Nombre no válido", ValidationName: Validators.maxLength.name },
    ], "Nombres");

    public Apellidos : IField = CreateField(new FormControl(), [
        { Validation: Validators.required, ErrorMessage: "Ingresa tu apellido." },
        { Validation: Validators.minLength(3), ErrorMessage: "Apellido no válido", ValidationName: Validators.minLength.name },
        { Validation: Validators.maxLength(50), ErrorMessage: "Apellido no válido", ValidationName: Validators.maxLength.name },
    ], "Apellidos");


    public Correo : IField = CreateField(new FormControl(), [
        { Validation: Validators.required, ErrorMessage: "Ingresa tu correo." },
        { Validation: Validators.minLength(3), ErrorMessage: "Correo no válido", ValidationName: Validators.minLength.name },
        { Validation: Validators.maxLength(100), ErrorMessage: "Correo no válido", ValidationName: Validators.maxLength.name },
        { Validation: Validators.email, ErrorMessage: "Correo no válido." },
    ], "Correo");

    public Usuario : IField = CreateField(new FormControl(), [
        { Validation: Validators.required, ErrorMessage: "Ingresa tu nombre de usuario." },
        { Validation: Validators.minLength(3), ErrorMessage: "Nombre de usuario no válido", ValidationName: Validators.minLength.name },
        { Validation: Validators.maxLength(50), ErrorMessage: "Nombre de usuario no válido", ValidationName: Validators.maxLength.name },
    ], "Usuario");

    public Contrasena : IField = CreateField(new FormControl(), [
        { Validation: Validators.required, ErrorMessage: "Ingresa tu contraseña" },
        { Validation: Validators.minLength(8), ErrorMessage: "Contraseña no válida", ValidationName: Validators.minLength.name },
        { Validation: Validators.maxLength(100), ErrorMessage: "Contraseña no válida", ValidationName: Validators.maxLength.name },
        { Validation: this.validatePasswordsMatch("ConfirmarContrasena"), ErrorMessage: "Las contraseñas deben coincidir. Intenta nuevamente", ValidationName: this.validatePasswordsMatch.name },
    ], "Contrasena");

    public ConfirmarContrasena : IField = CreateField(new FormControl(), [
        { Validation: Validators.required, ErrorMessage: "Confirma tu contraseña" },
        { Validation: Validators.maxLength(100), ErrorMessage: "Contraseña no válida", ValidationName: Validators.maxLength.name },
        { Validation: this.validatePasswordsMatch("Contrasena"), ErrorMessage: "Las contraseñas deben coincidir. Intenta nuevamente", ValidationName: this.validatePasswordsMatch.name },
    ], "ConfirmarContrasena");

    validatePasswordsMatch(controlNameToCompare : string) : ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
            let passwordToCompare = this.Register.get(controlNameToCompare)!;
            if(passwordToCompare.value?.length > 0) {
                if(control?.value !== passwordToCompare.value) {
                    return { "validatepasswordsmatch": true };
                } else if(passwordToCompare.touched) {
                    passwordToCompare.markAsUntouched();
                    passwordToCompare.setValue(control?.value);
                }
            }            
            return null;
        }
    }
}