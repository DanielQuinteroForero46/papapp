import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { UserRole } from 'src/app/domain/enums/auth-enum';
import { IUser } from 'src/app/domain/interfaces/interface-auth';
import { AlertPosition, AlertType } from 'src/app/domain/enums/alert-type-enum';
import { AlertService } from 'src/app/infraestructure/drive-adapter/alert-service/alert.service';
import { AuthService } from 'src/app/infraestructure/drive-adapter/auth-service/auth.service';
import { RegisterForm } from './validation/register-validation';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public Form : RegisterForm = new RegisterForm();
  public loading = false;

  constructor(private route: ActivatedRoute, private router: Router, private alertService: AlertService, private authService : AuthService) {
      // if (this.authService.accountValue?.data?.token) 
      //   this.router.navigate(['/']);
    }

  ngOnInit() {}

  SendRegister() {
    if(this.Form?.Register.invalid) return;
    this.loading = true;
    let userRegister : IUser = {
      firstName: this.Form.Nombres.Control.value,
      lastName: this.Form.Apellidos.Control.value,
      userRole: UserRole.Farmer,
      userLogin: {
        userName: this.Form.Usuario.Control.value,
        email: this.Form.Correo.Control.value,
        password: this.Form.Contrasena.Control.value,
      }
    }
    this.authService.Register(userRegister).pipe(first()).subscribe({
      next: (user : IUser) => {
        this.loading = false;
        this.alertService.showAlert({
          Type: AlertType.Success,
          Description: "¡REGISTRO EXITOSO!",
          Position: AlertPosition.UpCenter,
          ShowAsModal: true,
        });
        this.router.navigate(["/auth/login"]);
      },
      error: (error : any) => {
        this.alertService.showAlert({
          Type: AlertType.Error,
          Description: error,
          Position: AlertPosition.BottomRight,
        });
      }
    });
  }
}