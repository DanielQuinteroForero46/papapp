import { FormControl, FormGroup, Validators } from "@angular/forms";
import { CreateField } from "src/app/UI/common/helpers/form";
import { IField } from "../../../../../domain/interfaces/interface-validation";

export class LoginForm {
    public Login : FormGroup;
    constructor() {
        // DEFINE LOGIN FORM:
        this.Login = new FormGroup({
            UserName : this.UserName.Control,
            Password : this.Password.Control,
        });
    }

    // DEFINE FORM FIELDS WITH VALIDATIONS:
    public UserName : IField = CreateField(new FormControl(), [
        { Validation: Validators.required, ErrorMessage: "Ingresa tu nombre de usuario." },
        { Validation: Validators.minLength(3), ErrorMessage: "Nombre de usuario no válido", ValidationName: Validators.minLength.name },
        { Validation: Validators.maxLength(100), ErrorMessage: "Nombre de usuario no válido", ValidationName: Validators.maxLength.name },
    ], "UserName");

    public Password : IField = CreateField(new FormControl(), [
        { Validation: Validators.required, ErrorMessage: "Ingresa tu contraseña" },
        { Validation: Validators.minLength(8), ErrorMessage: "Contraseña no válida", ValidationName: Validators.minLength.name },
        { Validation: Validators.maxLength(100), ErrorMessage: "Contraseña no válida", ValidationName: Validators.maxLength.name },
    ], "Password");
}