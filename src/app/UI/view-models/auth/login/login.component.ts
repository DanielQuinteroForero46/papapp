import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginForm } from './validation/login-validation';
import { first } from 'rxjs/operators';
import { AlertPosition, AlertType } from '../../../../domain/enums/alert-type-enum';
import { AlertService } from '../../../../infraestructure/drive-adapter/alert-service/alert.service';
import { AuthService } from 'src/app/infraestructure/drive-adapter/auth-service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public Form : LoginForm = new LoginForm();
  public loading = false;

  constructor(private route: ActivatedRoute, private router: Router, 
    private authService : AuthService, private alertService: AlertService) {
      if (this.authService.accountValue?.userLogin?.token) 
        this.router.navigate(['/']);
    }

  ngOnInit() {}

  SendLogin() {
    if(this.Form?.Login.invalid) return;
    this.loading = true;
    this.authService.Login(this.Form.Login.value).pipe(first()).subscribe({
      next: () => {
        this.loading = false;
        this.router.navigate(['']);
      },
      error: (error : any) => {
        this.loading = false;
        this.alertService.showAlert({
          Type: AlertType.Error,
          Description: error,
          Position: AlertPosition.BottomRight,
        });
      }
    });
  }
}