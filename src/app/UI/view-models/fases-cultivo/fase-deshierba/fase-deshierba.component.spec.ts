import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FaseDeshierbaComponent } from './fase-deshierba.component';

describe('FaseDeshierbaComponent', () => {
  let component: FaseDeshierbaComponent;
  let fixture: ComponentFixture<FaseDeshierbaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FaseDeshierbaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FaseDeshierbaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
