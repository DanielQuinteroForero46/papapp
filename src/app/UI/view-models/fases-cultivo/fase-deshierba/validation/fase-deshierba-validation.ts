import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { IField, IFieldVal } from "src/app/domain/interfaces/interface-validation";
import { DeshierbaModel } from "src/app/domain/models/fases/fase-deshierba";
import { Insumo } from "src/app/domain/models/fases/insumo";
import { CreateField, MoneyGeneralValidators } from "src/app/UI/common/helpers/form";

export class FaseDeshierba {
    public FaseDeshierbaForm : FormGroup;
    private formBuilder : FormBuilder = new FormBuilder();
    public Fields : Array<IField> = [];
    public DeshierbaActiva : number = 0;

    constructor() {
        this.FaseDeshierbaForm = this.formBuilder.group({
            Deshierbas : this.formBuilder.array([])
        });
    }

    DeshierbasFormArray() : FormArray {
        return (this.FaseDeshierbaForm)? this.FaseDeshierbaForm.get("Deshierbas") as FormArray : new FormArray([]);
    }
    
    InsecticidasFormArray(Deshierba : AbstractControl) : FormArray {
        return (Deshierba)? Deshierba.get("Insecticidas") as FormArray : new FormArray([]);
    }

    PlaguicidasFormArray(Deshierba : AbstractControl) : FormArray {
        return (Deshierba)? Deshierba.get("Plaguicidas") as FormArray : new FormArray([]);
    }

    FungicidasFormArray(Deshierba : AbstractControl) : FormArray {
        return (Deshierba)? Deshierba.get("Fungicidas") as FormArray : new FormArray([]);
    }

    FertilizantesFormArray(Deshierba : AbstractControl) : FormArray {
        return (Deshierba)? Deshierba.get("Fertilizantes") as FormArray : new FormArray([]);
    }

    AgregarDeshierba(DeshierbaModel? : DeshierbaModel) {
        let DeshierbaForm : FormGroup = this.formBuilder.group({
            Insecticidas : this.formBuilder.array([]),
            Fungicidas : this.formBuilder.array([]),
            Plaguicidas : this.formBuilder.array([]),
            Fertilizantes : this.formBuilder.array([]),
            CostoFertilizanteGranulado : this.AgregarFieldDeshierba("Ingresa el costo por bulto", [], false, DeshierbaModel?.CostoFertilizanteGranulado).Control,
            CostoTransporteFertilizanteGranulado : this.AgregarFieldDeshierba("Ingresa el costo del transporte por bulto", [], false, DeshierbaModel?.CostoTransporteFertilizanteGranulado).Control,
            BultosFertilizanteGranulado : this.AgregarFieldDeshierba("Ingresa la cantidad de bultos", [], false, DeshierbaModel?.BultosFertilizanteGranulado).Control,
            TotalFertilizanteGranulado : this.AgregarFieldDeshierba("Ingresa el total", [], true, DeshierbaModel?.TotalFertilizanteGranulado).Control,
            TotalInsumos: this.AgregarFieldDeshierba("Ingresa el costo total de insumos", [], true, DeshierbaModel?.TotalInsumos).Control,
            CostoJornal: this.AgregarFieldDeshierba("Ingresa el valor por jornal", [], false, DeshierbaModel?.CostoJornal).Control,
            CostoAlimentacionJornal: this.AgregarFieldDeshierba("Ingresa el costo de alimentación", [], false, DeshierbaModel?.CostoAlimentacionJornal).Control,
            CantidadJornales: this.AgregarFieldDeshierba("Ingresa la cantidad de jornales", [], false, DeshierbaModel?.CantidadJornales).Control,
            TotalJornales: this.AgregarFieldDeshierba("", [], true, DeshierbaModel?.TotalJornales).Control,
            Otros: this.AgregarFieldDeshierba("", [], false, DeshierbaModel?.Otros).Control,
        });
        
        if(DeshierbaModel) {
            for(let Insecticida of DeshierbaModel!.Insecticidas!) {
                this.AgregarInsumo(this.InsecticidasFormArray(DeshierbaForm), Insecticida);
            }
            for(let Fungicida of DeshierbaModel!.Fungicidas!) {
                this.AgregarInsumo(this.FungicidasFormArray(DeshierbaForm), Fungicida);
            }
            for(let Fertilizante of DeshierbaModel!.Fertilizantes!) {
                this.AgregarInsumo(this.FertilizantesFormArray(DeshierbaForm), Fertilizante);
            }
        } else {
            this.AgregarInsumo(this.InsecticidasFormArray(DeshierbaForm));
            this.AgregarInsumo(this.FungicidasFormArray(DeshierbaForm));
            this.AgregarInsumo(this.PlaguicidasFormArray(DeshierbaForm));
            this.AgregarInsumo(this.FertilizantesFormArray(DeshierbaForm));
        }
        
        this.DeshierbasFormArray().push(DeshierbaForm);
    }

    AgregarInsumo(InsumoFormArray : FormArray, Insumo? : Insumo) {
        InsumoFormArray.push(this.formBuilder.group({
            Nombre: this.AgregarFieldDeshierba("Ingresa el nombre del insumo", [], false, Insumo?.Nombre).Control,
            Costo: this.AgregarFieldDeshierba("Ingresa el costo del insumo", MoneyGeneralValidators, false, Insumo?.Costo).Control,
            Litros: this.AgregarFieldDeshierba("Ingresa los litros del insumo", MoneyGeneralValidators, false, Insumo?.Litros).Control,
            Total: this.AgregarFieldDeshierba("Ingresa el total del insumo", MoneyGeneralValidators, true, Insumo?.Total).Control,
        }));
    }

    AgregarFieldDeshierba(ErrorMessage : string, CustomValidators? : Array<IFieldVal>, disabled : boolean = false, value? : any) : IField {
        let Field : IField = CreateField(new FormControl({ value: value, disabled: disabled }), 
        [{ Validation: Validators.required, ErrorMessage: ErrorMessage }].concat(CustomValidators || []));
        this.Fields.push(Field);
        return Field;
    }

    EliminarControlFormArray(FormArray : FormArray, IndexFormArray : number) {
        FormArray.removeAt(IndexFormArray);
    }

    GetErrorMessageByControl(Control : AbstractControl) : string {
        return this.Fields.find(c => c.Control === Control)?.ShowMsg!;
    }

    GetIndexByControl(Control : AbstractControl) {
        return this.Fields.findIndex(c => c.Control === Control);
    }
}