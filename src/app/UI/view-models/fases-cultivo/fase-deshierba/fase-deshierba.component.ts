import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AbstractControl, FormArray } from '@angular/forms';
import { FasesCultivo, TIPO_CALCULO } from 'src/app/domain/enums/fases-cultivo-enum';
import { IFases } from 'src/app/domain/interfaces/fases/interface-fases';
import { ICalculoForm } from 'src/app/domain/interfaces/interface-calculos-form';
import { Insumo } from 'src/app/domain/models/fases/insumo';
import { FasesService } from 'src/app/infraestructure/drive-adapter/fases-service/fases.service';
import { FaseDeshierba } from './validation/fase-deshierba-validation';

@Component({
  selector: 'app-fase-deshierba',
  templateUrl: './fase-deshierba.component.html',
  styleUrls: ['./fase-deshierba.component.scss']
})
export class FaseDeshierbaComponent implements OnInit {
  public FASES_CULTIVO = FasesCultivo;
  public fases : IFases | undefined;
  @Output() CargarFaseEvent = new EventEmitter();
  public Form : FaseDeshierba = new FaseDeshierba();
  public TIPO_CALCULO = TIPO_CALCULO;

  constructor(private fasesService: FasesService) {
    this.fases = this.fasesService.getFases;
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.PrecargarForm();
    setTimeout(() => {
      this.InputMoneyFormat();
    }, 2000)
  }

  async PrecargarForm() {
    if(this.fases!.FaseDeshierba && this.fases!.FaseDeshierba!.Deshierbas) {
      for(let Deshierbas of this.fases!.FaseDeshierba!.Deshierbas!) {
        this.Form.AgregarDeshierba(Deshierbas);
        this.CalcularTotalInsumos(this.Form.DeshierbasFormArray().length - 1);
      }
    } else {
      this.Form.AgregarDeshierba();
    }
  }

  CambiarFormDeshierba(formAnterior? : boolean) {
    if(formAnterior) {
      this.Form.DeshierbaActiva -= 1;
    } else {
      if(this.Form.DeshierbaActiva == (this.Form.DeshierbasFormArray().length - 1)) {
        this.Form.AgregarDeshierba();
      }
      this.Form.DeshierbaActiva += 1;
    }
    window.scrollTo(0, 0);
  }

  InputMoneyFormat() {
    document.querySelectorAll(".InputMoney").forEach((input) => {
      let IndexControl : number = parseInt(input.getAttribute("data-field-index")!);
      if(this.Form.Fields[IndexControl]) {
        let Control = this.Form.Fields[IndexControl].Control;
        if(!Control?.dirty) {
          this.TransformAmount(Control!.value, input);
        }
      }
    });
  }

  TransformAmount(value : string, input? : any) {
    let amount = this.fasesService.TransformAmount(value);
    if(input)
      input.value = amount;
    return amount;
  }

  ClearInput(event : any) {
    event!.target!.value = "";
  }

  CalcularTotalFertilizantesGran(DeshierbasFormGroup : AbstractControl) {
    return {
      Valores: [this.CalcularTotal({
          Valores: [DeshierbasFormGroup.get("CostoFertilizanteGranulado")!.value, DeshierbasFormGroup.get("CostoTransporteFertilizanteGranulado")!.value],
          TipoCalculo: TIPO_CALCULO.SUMA,
        }), 
        DeshierbasFormGroup.get("BultosFertilizanteGranulado")!.value
      ], 
      TipoCalculo: TIPO_CALCULO.PRODUCTO,
      TotalFormControl: DeshierbasFormGroup.get("TotalFertilizanteGranulado")!,
    }
  }

  CalcularTotalJornales(DeshierbasFormGroup : AbstractControl) {
    this.CalcularTotal({
      Valores: [this.CalcularTotal({
          Valores: [DeshierbasFormGroup.get("CostoJornal")!.value, DeshierbasFormGroup.get("CostoAlimentacionJornal")!.value],
          TipoCalculo: TIPO_CALCULO.SUMA,
        }), 
        DeshierbasFormGroup.get("CantidadJornales")!.value
      ], 
      TipoCalculo: TIPO_CALCULO.PRODUCTO,
      TotalFormControl: DeshierbasFormGroup.get("TotalJornales")!,
    });
  }

  CalcularTotalInsumo(InsumoFormGroup : AbstractControl, IndexDeshierbasFormArray : number) {
    this.CalcularTotalInsumos(IndexDeshierbasFormArray, {
      Valores: [InsumoFormGroup.get('Costo')!.value, InsumoFormGroup.get('Litros')!.value],
      TipoCalculo: TIPO_CALCULO.PRODUCTO,
      TotalFormControl: InsumoFormGroup.get('Total')!,
    });
  }

  CalcularTotalInsumos(IndexDeshierbasFormArray : number, CalculoInsumoForm? : ICalculoForm) {
    this.CalcularTotal(CalculoInsumoForm);
    let DeshierbaFormControl : AbstractControl = this.Form.DeshierbasFormArray().at(IndexDeshierbasFormArray)
    this.CalcularTotal({
      Valores: [
        this.TotalInsumos(this.Form.InsecticidasFormArray(DeshierbaFormControl).getRawValue()),
        this.TotalInsumos(this.Form.PlaguicidasFormArray(DeshierbaFormControl).getRawValue()),
        this.TotalInsumos(this.Form.FungicidasFormArray(DeshierbaFormControl).getRawValue()),
        this.TotalInsumos(this.Form.FertilizantesFormArray(DeshierbaFormControl).getRawValue()),
        DeshierbaFormControl.get("TotalFertilizanteGranulado")!.value
      ],
      TipoCalculo: TIPO_CALCULO.SUMA,
      TotalFormControl: DeshierbaFormControl.get("TotalInsumos")!,
    });
  }

  TotalInsumos(Insumos : Array<Insumo>) : number {
    let TotalInsumos : number = 0;
    Insumos.forEach((insumo : Insumo) => {
      let TotalInsumo = parseInt((insumo!.Total || "").toString());
      TotalInsumos += isNaN(TotalInsumo)? 0 : TotalInsumo;
    });
    return TotalInsumos;
  }

  CalcularTotal(CalculoForm? : ICalculoForm) : number {
    if(CalculoForm) {
      let total : number = this.fasesService.CalcularTotal(CalculoForm!.Valores, CalculoForm!.TipoCalculo!);
      if(CalculoForm!.TotalFormControl) {
        CalculoForm.TotalFormControl.setValue(total);
      }
      this.InputMoneyFormat();
      return total;
    }
    return 0;
  }

  EliminarDeshierba(DeshierbasIndexFormArray : number) {
    this.Form.EliminarControlFormArray(this.Form.DeshierbasFormArray(), DeshierbasIndexFormArray);
    this.Form.DeshierbaActiva = this.Form.DeshierbasFormArray().length - 1;
  }

  EliminarInsumo(InsumoFormArray : FormArray, IndexDeshierbasFormArray : number, InsumoIndexFormArray : number) {
    this.Form.EliminarControlFormArray(InsumoFormArray, InsumoIndexFormArray);
    this.CalcularTotalInsumos(IndexDeshierbasFormArray);
  }

  SendFaseDeshierbaForm(volver : boolean = false) {
    // if(this.Form.FaseDeshierba.valid) {
      this.fases!.FaseActiva = volver? FasesCultivo.TOTAL_FASE_SIEMBRA : FasesCultivo.TOTAL_FASE_CRECIMIENTO;
      this.fases!.FaseDeshierba = this.Form.FaseDeshierbaForm.getRawValue();
      this.AlmacenarFases();
    // }
  }

  Finalizar(volver : boolean = false) {
    let siguienteFase = volver? FasesCultivo.FASE_DESHIERBA : FasesCultivo.FASE_DESHIERBA;
    this.CargarFaseEvent.emit(siguienteFase);
  }

  AlmacenarFases() {
    window.scrollTo(0, 0);
    this.fasesService.AlmacenarFases(this.fases!);
    this.PrecargarForm();
  }

}