import { Component, OnInit } from '@angular/core';
import { FasesCultivo } from 'src/app/domain/enums/fases-cultivo-enum';
import { IFases } from 'src/app/domain/interfaces/fases/interface-fases';
import { FasesService } from 'src/app/infraestructure/drive-adapter/fases-service/fases.service';

@Component({
  selector: 'app-fases-cultivo',
  templateUrl: './fases-cultivo.component.html',
  styleUrls: ['./fases-cultivo.component.scss']
})
export class FasesCultivoComponent implements OnInit {
  public FASES_CULTIVO = FasesCultivo;
  public TituloFase : string = "";
  public fases : IFases | undefined;
  
  constructor(private fasesService: FasesService) { 
    this.fases = this.fasesService.getFases;
    this.CargarFase(this.fases.FaseActiva);
  }

  ngOnInit(): void {
  }

  CargarFase(Fase : FasesCultivo = FasesCultivo.LISTA_FASES) {
    this.fases!.FaseActiva = Fase;
    this.fasesService.AlmacenarFases(this.fases!);
    if(Fase)
      this.TituloFase = FasesCultivo[Fase].split("_")[1];

    switch(Fase) {
      case FasesCultivo.LISTA_FASES:
        this.TituloFase = "FASES DE CULTIVO";
        break;
      case FasesCultivo.TOTAL_FASE_SIEMBRA:
        this.TituloFase = "SIEMBRA";
        break;
    }
  }

}
