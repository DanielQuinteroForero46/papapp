import { FormControl, FormGroup, Validators } from "@angular/forms";
import { CreateField, MoneyGeneralValidators } from "src/app/UI/common/helpers/form";
import { IField } from "../../../../../domain/interfaces/interface-validation";

export class FaseSiembra1Form {
    public FaseSiembra1 : FormGroup;
    constructor() {
        this.FaseSiembra1 = new FormGroup({
            ArriendoTerreno : this.ArriendoTerreno.Control,
            AnalisisSuelo : this.AnalisisSuelo.Control,
            CostoLitroHerbicida : this.CostoLitroHerbicida.Control,
            LitrosHerbicida : this.LitrosHerbicida.Control,
            TotalHerbicida : this.TotalHerbicida.Control,
            CostoHoraArada : this.CostoHoraArada.Control,
            HorasArada : this.HorasArada.Control,
            TotalArada : this.TotalArada.Control,
            CostoRotoveteada : this.CostoRotoveteada.Control,
            HorasRotoveteada : this.HorasRotoveteada.Control,
            TotalRotoveteada : this.TotalRotoveteada.Control,
            CostoJornal1 : this.CostoJornal1.Control,
            CostoAlimentacionJornal1 : this.CostoAlimentacionJornal1.Control,
            CantidadJornales1 : this.CantidadJornales1.Control,
            TotalJornales1 : this.TotalJornales1.Control,
            TotalSiembra1 : this.TotalSiembra1.Control,
        });
    }

    public ArriendoTerreno : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa el costo del arriendo" }]), "ArriendoTerreno");

    public AnalisisSuelo : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa el costo de análisis de suelo" }]), "AnalisisSuelo");

    public CostoLitroHerbicida : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa el costo del Herbicida" }]), "CostoLitroHerbicida");

    public LitrosHerbicida : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa la cantidad de Herbicida" }]), "LitrosHerbicida");

    public TotalHerbicida : IField = CreateField(new FormControl({ value: "", disabled: true }), MoneyGeneralValidators, "TotalHerbicida");

    public CostoHoraArada : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa el costo de hora de arada" }]), "CostoHoraArada");

    public HorasArada : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa cantidad de horas de arada" }]), "HorasArada");

    public TotalArada : IField = CreateField(new FormControl({ value: "", disabled: true }), MoneyGeneralValidators, "TotalArada");

    public CostoRotoveteada : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa el costo de hora de rotoveteada" }]), "CostoRotoveteada");

    public HorasRotoveteada : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa cantidad de horas de rotoveteada" }]), "HorasRotoveteada");

    public TotalRotoveteada : IField = CreateField(new FormControl({ value: "", disabled: true }), MoneyGeneralValidators, "TotalRotoveteada");

    public CostoJornal1 : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa el valor por jornal" }]), "CostoJornal1");

    public CostoAlimentacionJornal1 : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa el costo de alimentación" }]), "CostoAlimentacionJornal1");

    public CantidadJornales1 : IField = CreateField(new FormControl(), 
    MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa la cantidad de jornales" }]), "CantidadJornales1");

    public TotalJornales1 : IField = CreateField(new FormControl({ value: "", disabled: true }), MoneyGeneralValidators, "TotalJornales1");

    public TotalSiembra1 : IField = CreateField(new FormControl({ value: "", disabled: true }), MoneyGeneralValidators, "TotalSiembra1");
}