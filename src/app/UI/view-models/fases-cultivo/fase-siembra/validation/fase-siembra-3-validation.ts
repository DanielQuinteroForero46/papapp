import { FormControl, FormGroup, Validators } from "@angular/forms";
import { CreateField, MoneyGeneralValidators } from "src/app/UI/common/helpers/form";
import { IField } from "../../../../../domain/interfaces/interface-validation";

export class FaseSiembra3Form {
    public FaseSiembra3 : FormGroup;
    constructor() {
        this.FaseSiembra3 = new FormGroup({
            CostoBultoSemilla : this.CostoBultoSemilla.Control,
            CostoTransporteSemilla : this.CostoTransporteSemilla.Control,
            BultosSemilla : this.BultosSemilla.Control,
            TotalSemilla : this.TotalSemilla.Control,
            CostoBultoFertilizante : this.CostoBultoFertilizante.Control,
            CostoTransporteFertilizante : this.CostoTransporteFertilizante.Control,
            BultosFertilizante : this.BultosFertilizante.Control,
            TotalFertilizante : this.TotalFertilizante.Control,
            CostoJornal3 : this.CostoJornal3.Control,
            CostoAlimentacionJornal3 : this.CostoAlimentacionJornal3.Control,
            CantidadJornales3 : this.CantidadJornales3.Control,
            TotalJornales3 : this.TotalJornales3.Control,
            TotalSiembra3 : this.TotalSiembra3.Control,
        });
    }

    public CostoBultoSemilla : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa el costo de semilla" }]), "CostoBultoSemilla");

    public CostoTransporteSemilla : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa el costo del transporte" }]), "CostoTransporteSemilla");
    
    public BultosSemilla : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa la cantidad de bultos" }]), "BultosSemilla");

    public TotalSemilla : IField = CreateField(new FormControl({ value: "", disabled: true }), MoneyGeneralValidators, "TotalSemilla");

    public CostoBultoFertilizante : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa el costo del Fertilizante" }]), "CostoBultoFertilizante");
    
    public CostoTransporteFertilizante : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa el costo del transporte" }]), "CostoTransporteFertilizante");

    public BultosFertilizante : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa la cantidad de bultos" }]), "BultosFertilizante");

    public TotalFertilizante : IField = CreateField(new FormControl({ value: "", disabled: true }), MoneyGeneralValidators, "TotalFertilizante");

    public CostoJornal3 : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa el valor por jornal" }]), "CostoJornal3");

    public CostoAlimentacionJornal3 : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa el costo de alimentación" }]), "CostoAlimentacionJornal3");

    public CantidadJornales3 : IField = CreateField(new FormControl(), 
    MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa la cantidad de jornales" }]), "CantidadJornales3");

    public TotalJornales3 : IField = CreateField(new FormControl({ value: "", disabled: true }), MoneyGeneralValidators, "TotalJornales3");

    public TotalSiembra3 : IField = CreateField(new FormControl({ value: "", disabled: true }), MoneyGeneralValidators, "TotalSiembra3");
}