import { FormControl, FormGroup, Validators } from "@angular/forms";
import { CreateField, MoneyGeneralValidators } from "src/app/UI/common/helpers/form";
import { IField } from "../../../../../domain/interfaces/interface-validation";

export class FaseSiembra2Form {
    public FaseSiembra2 : FormGroup;
    constructor() {
        this.FaseSiembra2 = new FormGroup({
            TipoTraccion : this.TipoTraccion.Control,
            CostoHoraSurcado : this.CostoHoraSurcado.Control,
            HorasSurcado : this.HorasSurcado.Control,
            TotalSurcado : this.TotalSurcado.Control,
            CostoInsecticida : this.CostoInsecticida.Control,
            LitrosInsecticida : this.LitrosInsecticida.Control,
            TotalInsecticida : this.TotalInsecticida.Control,
            CostoJornal2 : this.CostoJornal2.Control,
            CostoAlimentacionJornal2 : this.CostoAlimentacionJornal2.Control,
            CantidadJornales2 : this.CantidadJornales2.Control,
            TotalJornales2 : this.TotalJornales2.Control,
            TotalSiembra2 : this.TotalSiembra2.Control,
        });
    }

    public TipoTraccion : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Selecciona el tipo de tracción" }]), "TipoTraccion");

    public CostoHoraSurcado : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa el costo por hora del surcado" }]), "CostoHoraSurcado");

    public HorasSurcado : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa las horas de surcado" }]), "HorasSurcado");

    public TotalSurcado : IField = CreateField(new FormControl({ value: "", disabled: true }), MoneyGeneralValidators, "TotalSurcado");

    public CostoInsecticida : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa el costo del Insecticida" }]), "CostoInsecticida");

    public LitrosInsecticida : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa la cantidad de insecticida" }]), "LitrosInsecticida");

    public TotalInsecticida : IField = CreateField(new FormControl({ value: "", disabled: true }), MoneyGeneralValidators, "TotalInsecticida");

    public CostoJornal2 : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa el valor por jornal" }]), "CostoJornal2");

    public CostoAlimentacionJornal2 : IField = CreateField(new FormControl(), 
        MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa el costo de alimentación" }]), "CostoAlimentacionJornal2");

    public CantidadJornales2 : IField = CreateField(new FormControl(), 
    MoneyGeneralValidators.concat([{ Validation: Validators.required, ErrorMessage: "Ingresa la cantidad de jornales" }]), "CantidadJornales2");

    public TotalJornales2 : IField = CreateField(new FormControl({ value: "", disabled: true }), MoneyGeneralValidators, "TotalJornales2");

    public TotalSiembra2 : IField = CreateField(new FormControl({ value: "", disabled: true }), MoneyGeneralValidators, "TotalSiembra2");
}