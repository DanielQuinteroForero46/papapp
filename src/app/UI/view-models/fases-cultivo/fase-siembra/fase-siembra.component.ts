import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { FasesCultivo, TIPO_CALCULO } from 'src/app/domain/enums/fases-cultivo-enum';
import { IFases } from 'src/app/domain/interfaces/fases/interface-fases';
import { FaseSiembra1 } from 'src/app/domain/models/fases/fase-siembra1';
import { IField } from 'src/app/domain/interfaces/interface-validation';
import { FasesService } from 'src/app/infraestructure/drive-adapter/fases-service/fases.service';
import { FaseSiembra1Form } from './validation/fase-siembra-1-validation';
import { FaseSiembra2Form } from './validation/fase-siembra-2-validation';
import { FaseSiembra2 } from 'src/app/domain/models/fases/fase-siembra2';
import { FaseSiembra3Form } from './validation/fase-siembra-3-validation';
import { FaseSiembra3 } from 'src/app/domain/models/fases/fase-siembra3';

@Component({
  selector: 'app-fase-siembra',
  templateUrl: './fase-siembra.component.html',
  styleUrls: ['./fase-siembra.component.scss']
})
export class FaseSiembraComponent implements OnInit {
  public FASES_CULTIVO = FasesCultivo;
  public fases : IFases | undefined;
  @Output() CargarFaseEvent = new EventEmitter();
  public Form1 : FaseSiembra1Form = new FaseSiembra1Form();
  public Form2 : FaseSiembra2Form = new FaseSiembra2Form();
  public Form3 : FaseSiembra3Form = new FaseSiembra3Form();
  public TIPO_CALCULO = TIPO_CALCULO;

  constructor(private fasesService: FasesService) {
    this.fases = this.fasesService.getFases;
    this.PrecargarForm();
  }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

  TransformAmount(value : string, input? : any) {
    let amount = this.fasesService.TransformAmount(value);
    if(input)
      input.value = amount;
    return amount;
  }

  ClearInput(event : any) {
    event!.target!.value = "";
  }

  CalcularTotal(valores : Array<number>, TipoCalculo : TIPO_CALCULO, FieldTotal? : IField) {
    let total = this.fasesService.CalcularTotal(valores, TipoCalculo);
    FieldTotal?.Control.setValue(total);
    this.TransformAmount(FieldTotal?.Control?.value!, document.querySelector("#"+FieldTotal?.ElementID.toString()));
    return total;
  }

  async SendFaseSiembra1Form(volver : boolean = false) {
    if(this.Form1.FaseSiembra1.valid) {
      this.fases!.FaseActiva = volver? FasesCultivo.LISTA_FASES : FasesCultivo.FASE_SIEMBRA_2;
      this.fases!.FaseSiembra1 = this.Form1.FaseSiembra1.getRawValue();
      this.AlmacenarFases();
    }
  }

  SendFaseSiembra2Form(volver : boolean = false) {
    if(this.Form2.FaseSiembra2.valid) {
      this.fases!.FaseActiva = volver? FasesCultivo.FASE_SIEMBRA_1 : FasesCultivo.FASE_SIEMBRA_3;
      this.fases!.FaseSiembra2 = this.Form2.FaseSiembra2.getRawValue();
      this.AlmacenarFases();
    }
  }

  SendFaseSiembra3Form(volver : boolean = false) {
    if(this.Form3.FaseSiembra3.valid) {
      this.fases!.FaseActiva = volver? FasesCultivo.FASE_SIEMBRA_2 : FasesCultivo.TOTAL_FASE_SIEMBRA;
      this.fases!.FaseSiembra3 = this.Form3.FaseSiembra3.getRawValue();
      this.AlmacenarFases();
    }
  }

  Finalizar(volver : boolean = false) {
    let siguienteFase = volver? FasesCultivo.FASE_SIEMBRA_3 : FasesCultivo.FASE_CRECIMIENTO;
    if(this.Form1.FaseSiembra1.valid && this.Form2.FaseSiembra2.valid && this.Form3.FaseSiembra3.valid) {
      this.fases!.FaseSiembra1 = this.Form1.FaseSiembra1.getRawValue();
      this.fases!.FaseSiembra2 = this.Form2.FaseSiembra2.getRawValue();
      this.fases!.FaseSiembra3 = this.Form3.FaseSiembra3.getRawValue();
      this.CargarFaseEvent.emit(siguienteFase);
    }
  }

  AlmacenarFases() {
    window.scrollTo(0, 0);
    this.fasesService.AlmacenarFases(this.fases!);
    this.PrecargarForm();
  }

  async PrecargarForm() {
    this.Form1.FaseSiembra1 = await this.fasesService.PrecargarForm(this.Form1.FaseSiembra1, new FaseSiembra1(this.fases!.FaseSiembra1));
    this.Form2.FaseSiembra2 = await this.fasesService.PrecargarForm(this.Form2.FaseSiembra2, new FaseSiembra2(this.fases!.FaseSiembra2));
    this.Form3.FaseSiembra3 = await this.fasesService.PrecargarForm(this.Form3.FaseSiembra3, new FaseSiembra3(this.fases!.FaseSiembra3));
  }

}
