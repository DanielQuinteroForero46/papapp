import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FaseSiembraComponent } from './fase-siembra.component';

describe('FaseSiembraComponent', () => {
  let component: FaseSiembraComponent;
  let fixture: ComponentFixture<FaseSiembraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FaseSiembraComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FaseSiembraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
