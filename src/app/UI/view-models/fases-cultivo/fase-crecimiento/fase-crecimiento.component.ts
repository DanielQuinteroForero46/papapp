import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AbstractControl, FormArray } from '@angular/forms';
import { FasesCultivo, TIPO_CALCULO } from 'src/app/domain/enums/fases-cultivo-enum';
import { IFases } from 'src/app/domain/interfaces/fases/interface-fases';
import { ICalculoForm } from 'src/app/domain/interfaces/interface-calculos-form';
import { Insumo } from 'src/app/domain/models/fases/insumo';
import { FasesService } from 'src/app/infraestructure/drive-adapter/fases-service/fases.service';
import { FaseCrecimiento } from './validation/fase-crecimiento-validation';

@Component({
  selector: 'app-fase-crecimiento',
  templateUrl: './fase-crecimiento.component.html',
  styleUrls: ['./fase-crecimiento.component.scss']
})
export class FaseCrecimientoComponent implements OnInit {
  public FASES_CULTIVO = FasesCultivo;
  public fases : IFases | undefined;
  @Output() CargarFaseEvent = new EventEmitter();
  public Form : FaseCrecimiento = new FaseCrecimiento();
  public TIPO_CALCULO = TIPO_CALCULO;

  constructor(private fasesService: FasesService) {
    this.fases = this.fasesService.getFases;
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.PrecargarForm();
    setTimeout(() => {
      this.InputMoneyFormat();
    }, 2000)
  }

  async PrecargarForm() {
    // this.fases!.FaseCrecimiento! = new FaseCrecimientoModel({
    //   Fumigaciones: [
    //     new FumigacionModel({
    //       Insecticidas: [
    //         {
    //           Nombre: "Test",
    //           Costo: 1500,
    //           Litros: 3,
    //           Total: 4500
    //         },
    //       ]
    //     }),
    //   ]
    // });
    if(this.fases!.FaseCrecimiento && this.fases!.FaseCrecimiento!.Fumigaciones) {
      for(let Fumigacion of this.fases!.FaseCrecimiento!.Fumigaciones!) {
        this.Form.AgregarFumigacion(Fumigacion);
        this.CalcularTotalInsumos(this.Form.FumigacionesFormArray().length - 1);
      }
    } else {
      this.Form.AgregarFumigacion();
    }
  }

  CambiarFormFumigacion(formAnterior? : boolean) {
    if(formAnterior) {
      this.Form.FumigacionActiva -= 1;
    } else {
      if(this.Form.FumigacionActiva == (this.Form.FumigacionesFormArray().length - 1)) {
        this.Form.AgregarFumigacion();
      }
      this.Form.FumigacionActiva += 1;
    }
    window.scrollTo(0, 0);
  }

  InputMoneyFormat() {
    document.querySelectorAll(".InputMoney").forEach((input) => {
      let IndexControl : number = parseInt(input.getAttribute("data-field-index")!);
      if(this.Form.Fields[IndexControl]) {
        let Control = this.Form.Fields[IndexControl].Control;
        if(!Control?.dirty) {
          this.TransformAmount(Control!.value, input);
        }
      }
    });
  }

  TransformAmount(value : string, input? : any) {
    let amount = this.fasesService.TransformAmount(value);
    if(input)
      input.value = amount;
    return amount;
  }

  ClearInput(event : any) {
    event!.target!.value = "";
  }

  CalcularTotalJornales(FumigacionFormGroup : AbstractControl) {
    this.CalcularTotal({
      Valores: [this.CalcularTotal({
          Valores: [FumigacionFormGroup.get("CostoJornal")!.value, FumigacionFormGroup.get("CostoAlimentacionJornal")!.value],
          TipoCalculo: TIPO_CALCULO.SUMA,
        }), 
        FumigacionFormGroup.get("CantidadJornales")!.value
      ], 
      TipoCalculo: TIPO_CALCULO.PRODUCTO,
      TotalFormControl: FumigacionFormGroup.get("TotalJornales")!,
    });
  }

  CalcularTotalInsumo(InsumoFormGroup : AbstractControl, IndexFumigacionFormArray : number) {
    this.CalcularTotalInsumos(IndexFumigacionFormArray, {
      Valores: [InsumoFormGroup.get('Costo')!.value, InsumoFormGroup.get('Litros')!.value],
      TipoCalculo: TIPO_CALCULO.PRODUCTO,
      TotalFormControl: InsumoFormGroup.get('Total')!,
    });
  }

  CalcularTotalInsumos(IndexFumigacionFormArray : number, CalculoInsumoForm? : ICalculoForm) {
    this.CalcularTotal(CalculoInsumoForm);
    let FumigacionFormControl : AbstractControl = this.Form.FumigacionesFormArray().at(IndexFumigacionFormArray)
    this.CalcularTotal({
      Valores: [
        this.TotalInsumos(this.Form.InsecticidasFormArray(FumigacionFormControl).getRawValue()),
        this.TotalInsumos(this.Form.FungicidasFormArray(FumigacionFormControl).getRawValue()),
        this.TotalInsumos(this.Form.FertilizantesFormArray(FumigacionFormControl).getRawValue())
      ],
      TipoCalculo: TIPO_CALCULO.SUMA,
      TotalFormControl: FumigacionFormControl.get("TotalInsumos")!,
    });
  }

  TotalInsumos(Insumos : Array<Insumo>) : number {
    let TotalInsumos : number = 0;
    Insumos.forEach((insumo : Insumo) => {
      let TotalInsumo = parseInt((insumo!.Total || "").toString());
      TotalInsumos += isNaN(TotalInsumo)? 0 : TotalInsumo;
    });
    return TotalInsumos;
  }

  CalcularTotal(CalculoForm? : ICalculoForm) : number {
    if(CalculoForm) {
      let total : number = this.fasesService.CalcularTotal(CalculoForm!.Valores, CalculoForm!.TipoCalculo!);
      if(CalculoForm!.TotalFormControl) {
        CalculoForm.TotalFormControl.setValue(total);
      }
      this.InputMoneyFormat();
      return total;
    }
    return 0;
  }

  EliminarFumigacion(FumigacionIndexFormArray : number) {
    this.Form.EliminarControlFormArray(this.Form.FumigacionesFormArray(), FumigacionIndexFormArray);
    this.Form.FumigacionActiva = this.Form.FumigacionesFormArray().length - 1;
  }

  EliminarInsumo(InsumoFormArray : FormArray, IndexFumigacionFormArray : number, InsumoIndexFormArray : number) {
    this.Form.EliminarControlFormArray(InsumoFormArray, InsumoIndexFormArray);
    this.CalcularTotalInsumos(IndexFumigacionFormArray);
  }

  SendFaseCrecimientoForm(volver : boolean = false) {
    // if(this.Form.FaseCrecimientoForm.valid) {
      this.fases!.FaseActiva = volver? FasesCultivo.TOTAL_FASE_SIEMBRA : FasesCultivo.TOTAL_FASE_CRECIMIENTO;
      this.fases!.FaseCrecimiento = this.Form.FaseCrecimientoForm.getRawValue();
      this.AlmacenarFases();
    // }
  }

  Finalizar(volver : boolean = false) {
    let siguienteFase = volver? FasesCultivo.FASE_CRECIMIENTO : FasesCultivo.FASE_DESHIERBA;
    // if(this.Form1.FaseSiembra1.valid && this.Form2.FaseSiembra2.valid && this.Form3.FaseSiembra3.valid) {
      // this.fases!.FaseSiembra1 = this.Form1.FaseSiembra1.getRawValue();
      // this.fases!.FaseSiembra2 = this.Form2.FaseSiembra2.getRawValue();
      // this.fases!.FaseSiembra3 = this.Form3.FaseSiembra3.getRawValue();
      this.CargarFaseEvent.emit(siguienteFase);
    // }
  }

  AlmacenarFases() {
    window.scrollTo(0, 0);
    this.fasesService.AlmacenarFases(this.fases!);
    this.PrecargarForm();
  }
}
