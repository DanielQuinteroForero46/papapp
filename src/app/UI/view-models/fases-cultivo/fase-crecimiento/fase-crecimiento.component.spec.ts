import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FaseCrecimientoComponent } from './fase-crecimiento.component';

describe('FaseCrecimientoComponent', () => {
  let component: FaseCrecimientoComponent;
  let fixture: ComponentFixture<FaseCrecimientoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FaseCrecimientoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FaseCrecimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
