import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { IField, IFieldVal } from "src/app/domain/interfaces/interface-validation";
import { FumigacionModel } from "src/app/domain/models/fases/fase-crecimiento-fumigacion";
import { Insumo } from "src/app/domain/models/fases/insumo";
import { CreateField, MoneyGeneralValidators } from "src/app/UI/common/helpers/form";

export class FaseCrecimiento {
    public FaseCrecimientoForm : FormGroup;
    private formBuilder : FormBuilder = new FormBuilder();
    public Fields : Array<IField> = [];
    public FumigacionActiva : number = 0;

    constructor() {
        this.FaseCrecimientoForm = this.formBuilder.group({
            Fumigaciones : this.formBuilder.array([])
        });
    }

    FumigacionesFormArray() : FormArray {
        return (this.FaseCrecimientoForm)? this.FaseCrecimientoForm.get("Fumigaciones") as FormArray : new FormArray([]);
    }
    
    InsecticidasFormArray(Fumigacion : AbstractControl) : FormArray {
        return (Fumigacion)? Fumigacion.get("Insecticidas") as FormArray : new FormArray([]);
    }

    FungicidasFormArray(Fumigacion : AbstractControl) : FormArray {
        return (Fumigacion)? Fumigacion.get("Fungicidas") as FormArray : new FormArray([]);
    }

    FertilizantesFormArray(Fumigacion : AbstractControl) : FormArray {
        return (Fumigacion)? Fumigacion.get("Fertilizantes") as FormArray : new FormArray([]);
    }

    AgregarFumigacion(FumigacionModel? : FumigacionModel) {
        let FumigacionForm : FormGroup = this.formBuilder.group({
            Insecticidas : this.formBuilder.array([]),
            Fungicidas : this.formBuilder.array([]),
            Fertilizantes : this.formBuilder.array([]),
            TotalInsumos: this.AgregarFieldFumigacion("Ingresa el costo total de insumos", [], true, FumigacionModel?.TotalInsumos).Control,
            CostoJornal: this.AgregarFieldFumigacion("Ingresa el valor por jornal", [], false, FumigacionModel?.CostoJornal).Control,
            CostoAlimentacionJornal: this.AgregarFieldFumigacion("Ingresa el costo de alimentación", [], false, FumigacionModel?.CostoAlimentacionJornal).Control,
            CantidadJornales: this.AgregarFieldFumigacion("Ingresa la cantidad de jornales", [], false, FumigacionModel?.CantidadJornales).Control,
            TotalJornales: this.AgregarFieldFumigacion("", [], true, FumigacionModel?.TotalJornales).Control,
            Otros: this.AgregarFieldFumigacion("", [], false, FumigacionModel?.Otros).Control,
        });
        
        if(FumigacionModel) {
            for(let Insecticida of FumigacionModel!.Insecticidas!) {
                this.AgregarInsumo(this.InsecticidasFormArray(FumigacionForm), Insecticida);
            }
            for(let Fungicida of FumigacionModel!.Fungicidas!) {
                this.AgregarInsumo(this.FungicidasFormArray(FumigacionForm), Fungicida);
            }
            for(let Fertilizante of FumigacionModel!.Fertilizantes!) {
                this.AgregarInsumo(this.FertilizantesFormArray(FumigacionForm), Fertilizante);
            }
        } else {
            this.AgregarInsumo(this.InsecticidasFormArray(FumigacionForm));
            this.AgregarInsumo(this.FungicidasFormArray(FumigacionForm));
            this.AgregarInsumo(this.FertilizantesFormArray(FumigacionForm));
        }
        
        this.FumigacionesFormArray().push(FumigacionForm);
    }

    AgregarInsumo(InsumoFormArray : FormArray, Insumo? : Insumo) {
        InsumoFormArray.push(this.formBuilder.group({
            Nombre: this.AgregarFieldFumigacion("Ingresa el nombre del insumo", [], false, Insumo?.Nombre).Control,
            Costo: this.AgregarFieldFumigacion("Ingresa el costo del insumo", MoneyGeneralValidators, false, Insumo?.Costo).Control,
            Litros: this.AgregarFieldFumigacion("Ingresa los litros del insumo", MoneyGeneralValidators, false, Insumo?.Litros).Control,
            Total: this.AgregarFieldFumigacion("Ingresa el total del insumo", MoneyGeneralValidators, true, Insumo?.Total).Control,
        }));
    }

    AgregarFieldFumigacion(ErrorMessage : string, CustomValidators? : Array<IFieldVal>, disabled : boolean = false, value? : any) : IField {
        let Field : IField = CreateField(new FormControl({ value: value, disabled: disabled }), 
        [{ Validation: Validators.required, ErrorMessage: ErrorMessage }].concat(CustomValidators || []));
        this.Fields.push(Field);
        return Field;
    }

    EliminarControlFormArray(FormArray : FormArray, IndexFormArray : number) {
        FormArray.removeAt(IndexFormArray);
    }

    GetErrorMessageByControl(Control : AbstractControl) : string {
        return this.Fields.find(c => c.Control === Control)?.ShowMsg!;
    }

    GetIndexByControl(Control : AbstractControl) {
        return this.Fields.findIndex(c => c.Control === Control);
    }
}