import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FasesCultivoComponent } from './fases-cultivo.component';

describe('FasesCultivoComponent', () => {
  let component: FasesCultivoComponent;
  let fixture: ComponentFixture<FasesCultivoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FasesCultivoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FasesCultivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
