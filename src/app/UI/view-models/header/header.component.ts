import { Component, OnInit, EventEmitter , Output, Input, SimpleChanges, SimpleChange } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/infraestructure/drive-adapter/auth-service/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public expandirMenu : boolean = false;
  constructor(public router : Router, private authService : AuthService) { }

  ngOnInit() {
  }

  toggleSidebar(expandir : boolean) {
    this.expandirMenu = expandir;
  }

  logout(){
    this.authService.Logout();
  }

}
