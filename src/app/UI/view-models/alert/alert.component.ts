import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { Alert } from '../../../domain/models/alert/alert';
import { AlertPosition, AlertType } from '../../../domain/enums/alert-type-enum';
import { AlertService } from '../../../infraestructure/drive-adapter/alert-service/alert.service';

@Component({
  selector: 'app-alert',
  templateUrl: 'alert.component.html',
  styleUrls: ['alert.component.scss'],
})
export class AlertComponent implements OnInit, OnDestroy {
  @Input() id = 'default-alert';
  public alerts: Alert[] = [];
  private alertSubscription!: Subscription;
  private routeSubscription!: Subscription;
  public showAsModal?: boolean;

  constructor(private router: Router, private alertService: AlertService) {}

  ngOnInit() {
    // subscribe to new alert notifications
    this.alertSubscription = this.alertService.onAlert(this.id).subscribe((alertToShow: Alert) => {
      if (!alertToShow.Description) return;
      const alertTypeTitle = {
        [AlertType.Success]: 'CORRECTO',
        [AlertType.Error]: 'Ups...',
        [AlertType.Info]: 'INFORMACIÓN',
        [AlertType.Warning]: 'ADVERTENCIA',
      };

      this.showAsModal = alertToShow.ShowAsModal;
      if (!alertToShow.Title) {
        alertToShow.Title = alertTypeTitle[alertToShow.Type!];
      }

      if (!this.alerts.some((c) => c.Description === alertToShow.Description))
        this.alerts.push(alertToShow);

      if (alertToShow.AutoClose) {
        setTimeout(() => this.removeAlert(alertToShow), 3000);
      }
    });
    // clear alerts on location change
    this.routeSubscription = this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationStart) {
        this.alertService.clear(this.id);
      }
    });
  }

  ngOnDestroy() {
    // unsubscribe to avoid memory leaks
    this.alertSubscription.unsubscribe();
    this.routeSubscription.unsubscribe();
  }

  removeAlert(alert: Alert) {
    // check if already removed to prevent error on auto close
    if (!this.alerts.includes(alert)) return;
    this.alerts = this.alerts.filter((x) => x !== alert);
  }

  cssClasses(alert: Alert) {
    if (!alert) return;
    const classes = ['alert', 'alert-dismissable'];
    //Define type:
    const alertTypeClass = {
      [AlertType.Success]: 'alert alert-success',
      [AlertType.Error]: 'alert alert-danger',
      [AlertType.Info]: 'alert alert-info',
      [AlertType.Warning]: 'alert alert-warning',
    };
    classes.push(alertTypeClass[alert.Type!]);
    //Define position:
    const alertPositionClass = {
      [AlertPosition.UpLeft]: 'up-left',
      [AlertPosition.UpCenter]: 'up-center',
      [AlertPosition.UpRight]: 'up-right',
      [AlertPosition.BottomLeft]: 'bottom-left',
      [AlertPosition.BottomRight]: 'bottom-right',
      [AlertPosition.BottomRightLogin]: 'bottom-right-login',
    };

    if (alert.ShowAsModal)
      classes.push(alertPositionClass[AlertPosition.UpCenter]);
    else classes.push(alertPositionClass[alert.Position!]);

    return classes.join(' ');
  }
}