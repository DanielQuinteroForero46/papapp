import { Observable } from "rxjs";
import { IUser, IUserLogin } from "src/app/domain/interfaces/interface-auth";

export abstract class AuthGateway {
    abstract get accountValue(): IUser;
    abstract Register(user : IUser): Observable<IUser>;
    abstract Login(user : IUserLogin): Observable<IUser>;
    abstract Logout(): void;
    abstract GetLastActivity(): string;
    abstract SetLastActivity(): void;
    abstract RefreshToken(): void;
    abstract ValidateDownTime(): void;
}