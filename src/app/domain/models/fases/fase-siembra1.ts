export class FaseSiembra1 {
    public ArriendoTerreno?: number = 0;
    public AnalisisSuelo? : number = 0;
    public CostoLitroHerbicida? : number = 0;
    public LitrosHerbicida? : number = 0;
    public TotalHerbicida? : number = 0;
    public CostoHoraArada? : number = 0;
    public HorasArada? : number = 0;
    public TotalArada? : number = 0;
    public CostoRotoveteada? : number = 0;
    public HorasRotoveteada? : number = 0;
    public TotalRotoveteada? : number = 0;
    public CostoJornal1? : number = 0;
    public CostoAlimentacionJornal1? : number = 0;
    public CantidadJornales1? : number = 0;
    public TotalJornales1? : number = 0;
    public TotalSiembra1? : number = 0;

    constructor(init? : Partial<FaseSiembra1>) {
        Object.assign(this, init);
    }
}