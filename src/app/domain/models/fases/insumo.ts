export class Insumo {
    public Nombre? : string;
    public Costo? : number;
    public Litros? : number;
    public Total? : number;
    
    constructor(Insumo? : Insumo) {
        this.Nombre = Insumo?.Nombre || "";
        this.Costo = Insumo?.Costo || 0;
        this.Litros = Insumo?.Litros || 0;
        this.Total = Insumo?.Total || 0;
    }
}