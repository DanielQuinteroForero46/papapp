import { Insumo } from "./insumo";

export class DeshierbaModel {
    public Insecticidas? : Array<Insumo>;
    public Fungicidas? : Array<Insumo>;
    public Fertilizantes? : Array<Insumo>;
    public CostoFertilizanteGranulado? : number = 0;
    public CostoTransporteFertilizanteGranulado? : number = 0;
    public BultosFertilizanteGranulado? : number = 0;
    public TotalFertilizanteGranulado? : number = 0;
    public TotalInsumos? : number = 0;
    public CostoJornal? : number = 0;
    public CostoAlimentacionJornal? : number = 0;
    public CantidadJornales? : number = 0;
    public TotalJornales? : number = 0;
    public Otros? : number = 0;

    constructor(DeshierbaModel? : DeshierbaModel) {
        this.Insecticidas = DeshierbaModel?.Insecticidas || [new Insumo()];
        this.Fungicidas = DeshierbaModel?.Fungicidas || [new Insumo()];
        this.Fertilizantes = DeshierbaModel?.Fertilizantes || [new Insumo()];
        this.CostoFertilizanteGranulado = DeshierbaModel?.CostoFertilizanteGranulado || 0;
        this.CostoTransporteFertilizanteGranulado = DeshierbaModel?.CostoTransporteFertilizanteGranulado || 0;
        this.BultosFertilizanteGranulado = DeshierbaModel?.BultosFertilizanteGranulado || 0;
        this.TotalInsumos = DeshierbaModel?.TotalInsumos || 0;
        this.CostoJornal = DeshierbaModel?.CostoJornal || 0;
        this.CostoAlimentacionJornal = DeshierbaModel?.CostoAlimentacionJornal || 0;
        this.CantidadJornales = DeshierbaModel?.CantidadJornales || 0;
        this.TotalJornales = DeshierbaModel?.TotalJornales || 0;
        this.Otros = DeshierbaModel?.Otros || 0;
    }
}