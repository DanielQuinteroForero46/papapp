export class FaseSiembra2 {
    public TipoTraccion?: number = 0;
    public CostoHoraSurcado?: number = 0;
    public HorasSurcado?: number = 0;
    public TotalSurcado?: number = 0;
    public CostoInsecticida?: number = 0;
    public LitrosInsecticida?: number = 0;
    public TotalInsecticida?: number = 0;
    public CostoJornal2? : number = 0;
    public CostoAlimentacionJornal2? : number = 0;
    public CantidadJornales2? : number = 0;
    public TotalJornales2? : number = 0;
    public TotalSiembra2? : number = 0;

    constructor(init? : Partial<FaseSiembra2>) {
        Object.assign(this, init);
    }
}