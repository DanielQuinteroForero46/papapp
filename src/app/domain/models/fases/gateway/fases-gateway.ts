import { Observable } from "rxjs";
import { IFases } from "src/app/domain/interfaces/fases/interface-fases";

export abstract class FasesGateway {
    abstract get getFases() : IFases;
    abstract AlmacenarFases(fases : IFases) : boolean;
}