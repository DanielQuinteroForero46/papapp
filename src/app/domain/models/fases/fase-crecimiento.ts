import { FumigacionModel } from "./fase-crecimiento-fumigacion";

export class FaseCrecimientoModel {
    public Fumigaciones? : Array<FumigacionModel>;

    constructor(FaseCrecimiento? : FaseCrecimientoModel) {
        this.Fumigaciones = FaseCrecimiento?.Fumigaciones || [new FumigacionModel()];
    }
}