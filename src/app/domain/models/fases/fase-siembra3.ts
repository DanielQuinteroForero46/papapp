export class FaseSiembra3 {
    public CostoBultoSemilla? : number = 0;
    public CostoTransporteSemilla? : number = 0;
    public BultosSemilla? : number = 0;
    public TotalSemilla? : number = 0;
    public CostoBultoFertilizante? : number = 0;
    public CostoTransporteFertilizante? : number = 0;
    public BultosFertilizante? : number = 0;
    public TotalFertilizante? : number = 0;
    public CostoJornal3? : number = 0;
    public CostoAlimentacionJornal3? : number = 0;
    public CantidadJornales3? : number = 0;
    public TotalJornales3? : number = 0;
    public TotalSiembra3? : number = 0;

    constructor(init? : Partial<FaseSiembra3>) {
        Object.assign(this, init);
    }
}