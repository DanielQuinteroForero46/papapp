import { Insumo } from "./insumo";

export class FumigacionModel {
    public Insecticidas? : Array<Insumo>;
    public Fungicidas? : Array<Insumo>;
    public Fertilizantes? : Array<Insumo>;
    public TotalInsumos? : number = 0;
    public CostoJornal? : number = 0;
    public CostoAlimentacionJornal? : number = 0;
    public CantidadJornales? : number = 0;
    public TotalJornales? : number = 0;
    public Otros? : number = 0;

    constructor(FumigacionModel? : FumigacionModel) {
        this.Insecticidas = FumigacionModel?.Insecticidas || [new Insumo()];
        this.Fungicidas = FumigacionModel?.Fungicidas || [new Insumo()];
        this.Fertilizantes = FumigacionModel?.Fertilizantes || [new Insumo()];
        this.TotalInsumos = FumigacionModel?.TotalInsumos || 0;
        this.CostoJornal = FumigacionModel?.CostoJornal || 0;
        this.CostoAlimentacionJornal = FumigacionModel?.CostoAlimentacionJornal || 0;
        this.CantidadJornales = FumigacionModel?.CantidadJornales || 0;
        this.TotalJornales = FumigacionModel?.TotalJornales || 0;
        this.Otros = FumigacionModel?.Otros || 0;
    }
}