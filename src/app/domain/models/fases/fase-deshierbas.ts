import { DeshierbaModel } from "./fase-deshierba";

export class FaseDeshierbaModel {
    public Deshierbas? : Array<DeshierbaModel>;

    constructor(FaseDeshierba? : FaseDeshierbaModel) {
        this.Deshierbas = FaseDeshierba?.Deshierbas || [new DeshierbaModel()];
    }
}