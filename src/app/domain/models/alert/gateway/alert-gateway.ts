import { Observable } from "rxjs";
import { Alert } from "../alert";

export abstract class AlertGateway {
    abstract onAlert(id: any): Observable<Alert>;
    abstract showAlert(messageAlert : Alert): void;
}