import { AlertPosition, AlertType } from "../../enums/alert-type-enum";

export class Alert {
    Id?: string;
    Title?: string;
    Type?: AlertType;
    Description?: string;
    Position?: AlertPosition;
    AutoClose?: boolean;
    ShowAsModal?: boolean;

    constructor(init?:Partial<Alert>) {
        Object.assign(this, init);
    }

}