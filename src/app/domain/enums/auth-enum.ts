export enum UserRole {
    Administrator = 1,
    Farmer
}

export enum UserState {
    Active = 1,
    Blocked
}

export enum PasswordType {
    Active = 1,
    Temporary,
    Blocked
}