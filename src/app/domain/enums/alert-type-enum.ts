export enum AlertType {
  Success,
  Error,
  Info,
  Warning,
}

export enum AlertPosition {
  UpLeft,
  UpRight,
  UpCenter,
  BottomLeft,
  BottomRight,
  BottomRightLogin,
}
