import { AbstractControl } from "@angular/forms";
import { TIPO_CALCULO } from "../enums/fases-cultivo-enum";

export interface ICalculoForm {
    Valores : Array<number>,
    TipoCalculo? : TIPO_CALCULO,
    TotalFormControl? : AbstractControl, 
    TotalElementID? : string,
}