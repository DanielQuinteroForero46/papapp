import { ValidatorFn, FormControl } from '@angular/forms';

export interface IField {
    ElementID : string,
    Control : FormControl,
    Validations : Array<IFieldVal>,
    ShowMsg : string
}

export interface IFieldVal {
    ValidationName?: string,
    ErrorMessage: string,
    Validation: ValidatorFn
}