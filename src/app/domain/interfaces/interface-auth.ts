import { PasswordType, UserRole, UserState } from "../enums/auth-enum";

export interface IUser {
    id?: number
    userRole?: UserRole
    userState?: UserState
    firstName: string
    lastName: string
    userLogin: IUserLogin
}

export interface IUserLogin {
    userName: string,
    email?: string,
    password?: string,
    passwordType?: PasswordType,
    token?: string,
    refreshToken?: string
}