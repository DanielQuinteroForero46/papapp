import { FasesCultivo } from "../../enums/fases-cultivo-enum";
import { FaseCrecimientoModel } from "../../models/fases/fase-crecimiento";
import { FaseDeshierbaModel } from "../../models/fases/fase-deshierbas";
import { FaseSiembra1 } from "../../models/fases/fase-siembra1";
import { FaseSiembra2 } from "../../models/fases/fase-siembra2";
import { FaseSiembra3 } from "../../models/fases/fase-siembra3";

export interface IFases {
    FaseActiva? : FasesCultivo,
    FaseSiembra1? : FaseSiembra1,
    FaseSiembra2? : FaseSiembra2,
    FaseSiembra3? : FaseSiembra3,
    FaseCrecimiento? : FaseCrecimientoModel,
    FaseDeshierba? : FaseDeshierbaModel,
}